<?php

/**
 * @file
 * Chatpirate.settings.inc.
 */

require 'chatpirateapi.class.inc';

/**
 * ChatPirate Settings Form.
 *
 * @return mixed
 *   Method which controls form.
 */
function chatpirate_settings_form() {

  $module_dir = drupal_get_path('module', 'chatpirate');

  drupal_add_css($module_dir . '/css/chatpirate.css');
  drupal_add_js(
    $module_dir . '/js/chatpirate.js', array(
      'type' => 'file',
      'scope' => 'footer',
    )
  );

  $companyid = variable_get('chatpirate_companyid', FALSE);
  $token = variable_get('chatpirate_token', FALSE);
  $email = variable_get('chatpirate_email', FALSE);

  if ($companyid && $token && $email) {
    return chatpirate_form_reset();
  }
  else {
    return chatpirate_form_create();
  }
}

/**
 * Module Form creator.
 *
 * @return array
 *   Returns $form array
 */
function chatpirate_form_create() {

  $form['login'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="chatpirate-logo"></div><span class="chatpirate-header-text">' . t('Connect Drupal with Your ChatPirate Account') . '</span>
                      <div id="login-form" class="chatpirate-form">',
    '#suffix' => '</div><span id="create_account">' . t("Haven't an account yet?") . '<a href="//chatpirate.com/signup/createdType/drupal" target="_blank"> ' . t("Sign up") . '</a></span>',
  );

  $form['login']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#size' => 40,
    '#maxlength' => 70,
    '#required' => TRUE,
    '#attributes' => array(
      'autocomplete' => 'off',
    ),
  );

  $form['login']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#size' => 40,
    '#maxlength' => 70,
    '#required' => TRUE,
    '#attributes' => array(
      'autocomplete' => 'off',
    ),
  );

  $form['login']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Connect'),
    '#validate' => array('chatpirate_settings_validate'),
    '#submit' => array('chatpirate_settings_save'),
  );

  return $form;
}

/**
 * Validate settings.
 *
 * @var $form
 * @var $form_state
 */
function chatpirate_settings_validate($form, $form_state) {

  if (!filter_var($form_state['values']['email'], FILTER_VALIDATE_EMAIL)) {
    form_set_error(
      'email',
      $form_state['values']['email'] . ' ' . t("is not a valid email address in the basic format local-part@hostname")
    );
  }
}

/**
 * Save Settings.
 *
 * @var $form
 * @var $form_state
 *
 * @return string
 *   Returns drupal message.
 */
function chatpirate_settings_save($form, $form_state) {

  $api = new ChatPirateApi();

  $api->authSet($form_state['values']['email'], $form_state['values']['password']);
  $login_by_password = $api->cpLoginByPassword();

  if (isset($login_by_password->error->message)) {
    drupal_set_message($login_by_password->error->message, 'error');
  }
  elseif ($login_by_password == FALSE) {
    chatpirate_something_went_wrong();
  }
  else {
    if (isset($login_by_password->data->companyId) && !empty($login_by_password->data->companyId)
          && isset($login_by_password->data->token) && !empty($login_by_password->data->token)
          && isset($login_by_password->data->email) && !empty($login_by_password->data->email)
      ) {

      $companyid = $login_by_password->data->companyId;
      $token = $login_by_password->data->token;
      $email = $login_by_password->data->email;

      variable_set('chatpirate_companyid', $companyid);
      variable_set('chatpirate_token', $token);
      variable_set('chatpirate_email', $email);

      return drupal_set_message(t('You have successfully connected your ChatPirate account!'), 'status');

    }
    else {
      chatpirate_something_went_wrong();
    }
  }
}

/**
 * Sets variables to null.
 */
function chatpirate_settings_reset() {

  variable_set('chatpirate_companyid', '');
  variable_set('chatpirate_token', '');
  variable_set('chatpirate_email', '');
}

/**
 * Displays error message.
 */
function chatpirate_something_went_wrong() {

  return drupal_set_message(t('Whoops, looks like something went wrong.'), 'error');
}

/**
 * Module form-reset creator.
 *
 * @return array
 *   Returns $form array
 */
function chatpirate_form_reset() {

  $api = new ChatPirateApi();
  $token = variable_get('chatpirate_token', FALSE);
  $email = variable_get('chatpirate_email', FALSE);
  $go_to_app_url = $api->cpLoginByToken($email, $token);

  $form['login'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="chatpirate-logo"></div>
                      <span class="gototext">' . t("In order to launch ChatPirate, click 'Go to App'.") . '</span>
                      <a class="btn_chatpirate_login" target="_blank" href="' . $go_to_app_url . '">' . t("Go to App") . '</a>
                      <span id="logout">' . t("Sign in to a different account?") . ' <a href="#">' . t("Log out") . '</a></span>
                      <div id="logout-form" class="chatpirate-form">',
    '#suffix' => '</div><span id="create_account_after_logout">' . t("Create new account") . ' <a href="//chatpirate.com/signup/createdType/drupal" target="_blank"> ' . t("Sign up") . '</a></span>',
  );

  $form['login']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#size' => 40,
    '#maxlength' => 70,
    '#required' => TRUE,
    '#attributes' => array(
      'autocomplete' => 'off',
    ),
  );

  $form['login']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#size' => 40,
    '#maxlength' => 70,
    '#required' => TRUE,
    '#attributes' => array(
      'autocomplete' => 'off',
    ),
  );

  $form['login']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Connect'),
    '#validate' => array('chatpirate_settings_validate'),
    '#submit' => array('chatpirate_settings_save'),
  );

  return $form;
}
