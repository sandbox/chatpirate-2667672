/**
 * @file
 * Chatpirate.js.
 */

(function($){
    Drupal.behaviors.chatpirate = {
        attach: function (context, settings) {
            $('#logout a').click(
                function () {
                    $('#logout-form').fadeIn('slow');
                    $('#create_account_after_logout').fadeIn('slow');
                }
            );
            $('#chatpirate-settings-form').submit(function(e){
                var email     = $(this).find('input[name=email]'),
                    pass      = $(this).find('input[name=password]'),
                    email_val = /^([a-zA-Z0-9_\.\+\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
                    e_pref    = '<div class="input_error">',
                    e_suff    = '</div>',
                    errors    = false;

                $(this).find('.input_error').remove();
                $(this).find('input').removeClass('error');

                if(email.val().length == 0){
                    email.addClass('error');
                    email.parent().append(e_pref + Drupal.t('E-mail can not be empty') + e_suff);
                    errors = true;
                }
                else if(!email_val.test(email.val())){
                    email.addClass('error');
                    email.parent().append(e_pref + Drupal.t('E-mail is not valid') + e_suff);
                    errors = true;
                }

                if(pass.val().length == 0){
                    pass.addClass('error');
                    pass.parent().append(e_pref + Drupal.t('Password can not be empty') + e_suff);
                    errors = true;
                }

                if(errors == true)
                    e.preventDefault();
            });
        }
    };
})(jQuery);
